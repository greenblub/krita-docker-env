#!/bin/bash

usage=\
"Usage: $(basename "$0") [IMAGE_NAME] [CONTAINER_NAME]\n
Run container for the specified image\n
\n
where:\n
    -h,      --help              show this help text\n
\n
If no IMAGE_NAME or CONTAINER_NAME is given, they are\n
deduced from the current directory name
"

# Call getopt to validate the provided input. 
options=$(getopt -o h --long help -- "$@")
[ $? -eq 0 ] || { 
    echo "Incorrect options provided"
    exit 1
}
eval set -- "$options"
while true; do
    case "$1" in
    -h | --help)
        echo -e $usage >&2
        exit 1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${DIR}/find_default_container_file.inc

IMAGE_NAME=${@:$OPTIND:1}

if [[ -z ${IMAGE_NAME} ]]; then
    IMAGE_NAME=$(basename $(realpath $DIR/..))
    echo Auto-select image name: ${IMAGE_NAME}
fi

CONTAINER_NAME=${@:$OPTIND+1:1}

if [[ -z ${CONTAINER_NAME} ]]; then
    CONTAINER_NAME=${IMAGE_NAME}
    echo Auto-select container name: ${CONTAINER_NAME}
fi

NVIDIA_OPTS=
if [[ -e /dev/nvidiactl ]]; then
   NVIDIA_OPTS+="--device /dev/nvidia0 --device /dev/nvidiactl --device /dev/nvidia-uvm"
fi

if [[ -d $(pwd)/persistent/downloads ]]; then
    EXTRA_DOCKER_MOUNTS="-v $(pwd)/persistent/downloads:/home/appimage/appimage-workspace/downloads:rw"
fi

KVM_OPTS=
if [[ -e /dev/kvm ]]; then
   KVM_OPTS+="--device /dev/kvm"
fi

tryCreatePulseaudioSocket
tryCreateXAuthCookieFile
runUserInitializationHooks

${DOCKER_BINARY} run -P -t -d \
  $EXTRA_DOCKER_MOUNTS \
  -v $(pwd)/persistent/:/home/appimage/persistent/:rw \
  -v /tmp/.X11-unix/:/tmp/.X11-unix \
  -v /tmp/krita-docker-xauth:/tmp/krita-docker-xauth:ro \
  -v /etc/localtime:/etc/localtime:ro \
  -e DISPLAY \
  -e XAUTHORITY=/tmp/krita-docker-xauth \
  -h $HOSTNAME \
  --cap-add=SYS_PTRACE \
  --security-opt seccomp=unconfined \
  --security-opt label=type:container_runtime_t \
  --device /dev/dri \
  --env PULSE_SERVER=unix:/tmp/pulseaudio-docker.socket \
  --env PULSE_COOKIE=/tmp/pulseaudio-docker.cookie \
  --volume /tmp/pulseaudio-docker.socket:/tmp/pulseaudio-docker.socket \
  --volume /tmp/pulseaudio.client.conf:/etc/pulse/client.conf \
  $NVIDIA_OPTS \
  $KVM_OPTS \
  --name $CONTAINER_NAME \
  $IMAGE_NAME || exit 1

if [ ! -f .container_name ]; then
    echo $CONTAINER_NAME > .container_name
fi

