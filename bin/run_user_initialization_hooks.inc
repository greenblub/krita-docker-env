#!/bin/bash

function runUserInitializationHooks {
    local DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    local HOOKSDIR=$(realpath "$DIR/../user-hooks.d")

    if [[ -d $HOOKSDIR ]]; then
        for file in `find $HOOKSDIR -executable -type f | sort`; do
            echo "Running user hook: $file"
            $file
        done
    fi

}
